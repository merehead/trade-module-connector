<?php


namespace MereHead\TradeModuleConnector\TradeServices;

trait PVCScreener
{
    public function getPVCBuySellVolume(string $pairCode)
    {
        $body = [];
        return $this->makeCallGuzzle('GET', 'pvc_screener/'.$pairCode.'/buy_sell_volume', $body);
    }

    public function getPriceAndVolumeDynamic(string $pairCode)
    {
        $body = [];
        return $this->makeCallGuzzle('GET', 'pvc_screener/'.$pairCode.'/volume_price_dynamic', $body);
    }

    public function getPVCChartDataMinute(string $fsym, string $tsym, $limit)
    {
        $body = [
            'fsym' => $fsym,
            'tsym' => $tsym,
//            'toTs' => $toTs,
            'limit' => $limit
        ];
        return $this->makeCallGuzzle('GET', 'pvc_screener/chart/histominute', $body);
    }

    public function getPVCChartDataHour(string $fsym, string $tsym, $limit)
    {
        $body = [
            'fsym' => $fsym,
            'tsym' => $tsym,
//            'toTs' => $toTs,
            'limit' => $limit
        ];
        return $this->makeCallGuzzle('GET', 'pvc_screener/chart/histohour', $body);
    }

    public function getPVCChartDataDay(string $fsym, string $tsym, $limit)
    {
        $body = [
            'fsym' => $fsym,
            'tsym' => $tsym,
//            'toTs' => $toTs,
            'limit' => $limit
        ];
        return $this->makeCallGuzzle('GET', 'pvc_screener/chart/histoday', $body);
    }
}
