<?php

namespace MereHead\TradeModuleConnector\TradeServices;

trait MarketDataService
{
    //new method
    public function getTickerData(bool $full = false)
    {
        $body = [
            'full' => $full
        ];

        return $this->makeCallGuzzle('GET', 'ticker_data', $body);
    }

    public function getOrdersBookApi(string $pair, int $limit = null)
    {
        $body = [
            'pair'  => $pair,
            'limit' => $limit,
        ];

        return $this->makeCallGuzzle('GET', 'order_book_depth', $body);
    }

    public function getExchangeInfo()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'exchange_info', $body);
    }

    public function getHistoricalTrades(string $pair, int $limit = null, int $fromId = null)
    {
        $body = [
            'pair'    => $pair,
            'limit'   => $limit,
            'from_id' => $fromId,
        ];

        return $this->makeCallGuzzle('GET', 'historical_trades', $body);
    }

    public function getNewTrades(string $pair, string $type = null, int $limit = null)
    {
        $body = [
            'pair'    => $pair,
            'type'    => $type,
            'limit'   => $limit,
        ];

        return $this->makeCallGuzzle('GET', 'new_trades', $body);
    }
}