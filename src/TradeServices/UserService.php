<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:13 PM
 */

namespace MereHead\TradeModuleConnector\TradeServices;


trait UserService
{

    /**
     * Command for listening : create_account
     * Create user account
     * @param int $user_id
     * @param int|null $parent_id
     * @return mixed
     */
    public function createAccount(int $user_id, int $parent_id = null)
    {
        $body = [
            'account_id' => $user_id,
            'parent_id' => $parent_id,
        ];

        return $this->makeCallGuzzle('POST', 'account', $body);
    }


    /**
     *  Command for listening : create_user_fee
     * Create users fee
     * @param $user_id
     * @return array
     */
    public function createUserFee($user_id): array
    {
        $body = [
            'account_id' => $user_id,
        ];

        return $this->makeCallGuzzle('POST', 'account_fee', $body);
    }


    public function getStatusOfUsingTokenForFee(int $user_id)
    {
        $body = [
            'account_id' => $user_id,
        ];

        return $this->makeCallGuzzle('GET', 'use_token_for_fee', $body);
    }

    public function updateStatusOfUsingTokenForFee(int $user_id, int $use_token_for_fee)
    {
        $body = [
            'account_id'        => $user_id,
            'use_token_for_fee' => $use_token_for_fee,
        ];

        return $this->makeCallGuzzle('PUT', 'use_token_for_fee', $body);
    }
}