<?php


namespace MereHead\TradeModuleConnector\TradeServices;

trait ProfitService
{
    public function getProfit()
    {
        $body = [];
        return $this->makeCallGuzzle('GET', 'profit', $body);
    }

    public function getUserTotalReferralBonuses(int $account_id)
    {
        $body = [
            'account_id' => $account_id,
        ];

        return $this->makeCallGuzzle('GET', 'user_bonuses', $body);
    }

    public function getReferralBonusesByUsers(array $users)
    {
        $body = [
            'users' => $users,
        ];

        return $this->makeCallGuzzle('GET', 'bonuses_by_user', $body);
    }

    public function getRealProfit(string $assetId = null)
    {
        $body = [
            'asset_id' => $assetId,
        ];

        return $this->makeCallGuzzle('GET', 'profit_real', $body);
    }

    public function increaseRealProfit(string $assetId, float $amount)
    {
        $body = [
            'asset_id' => $assetId,
            'amount'   => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'profit_increase', $body);
    }

    public function sendDividends(string $assetId, int $userId, float $amount)
    {
        $body = [
            'asset_id' => $assetId,
            'user_id'  => $userId,
            'amount'   => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'send_dividends', $body);
    }

    public function getAllDividends(int $currentPage = 1, int $perPage = 15)
    {
        $body = [
            'current_page' => $currentPage,
            'per_page'     => $perPage,
        ];

        return $this->makeCallGuzzle('GET', 'dividends', $body);
    }
}
