<?php


namespace MereHead\TradeModuleConnector\TradeServices;

trait TestService
{
    public function test($testParam)
    {
        $body = [
            'test_param' => $testParam,
        ];

        return $this->makeCallGuzzle('GET', 'test', $body);
    }
}
