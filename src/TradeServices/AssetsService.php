<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:15 PM
 */

namespace MereHead\TradeModuleConnector\TradeServices;


trait AssetsService
{


    /**
     * Command for listening : get_assets
     * Get list assets
     * @param bool $all
     * @return mixed
     */
    public function getAssets($all = false)
    {
        $body = [
            'all' => $all
        ];

        return $this->makeCallGuzzle('GET', 'assets', $body);
    }

    /**
     * Command for listening : get_assets_pairs
     * Get list assets pairs
     * @param array|null $params
     * @param bool $active
     * @return mixed
     */
    public function getAssetsPairs(array $params = null, $active = false)
    {
        $body = [
            'params' => $params,
            'active' => $active,
        ];

        return $this->makeCallGuzzle('GET', 'assets_pairs', $body);
    }

    /**
     * Return asset pair price
     * @param string $baseAssetCode
     * @param string $quoteAssetCode
     * @return mixed
     */
    public function getAssetPairPrice(string $baseAssetCode, string $quoteAssetCode)
    {
        $body = [
            'base_asset_code' => $baseAssetCode,
            'quote_asset_code' => $quoteAssetCode,
        ];

        return $this->makeCallGuzzle('GET', 'asset_pair_price', $body);
    }

    public function addAssetPair(string $baseAssetCode, string $quoteAssetCode)
    {
        $body = [
            'base_asset_code'  => $baseAssetCode,
            'quote_asset_code' => $quoteAssetCode,
        ];

        return $this->makeCallGuzzle('POST', 'add_asset_pair', $body);
    }

    public function updateAssetPair(string $assetPairCode, int $active)
    {
        $body = [
            'asset_pair_code'  => $assetPairCode,
            'active'           => $active,
        ];

        return $this->makeCallGuzzle('PUT', 'update_asset_pair', $body);
    }

    public function updateMakerAssetPair(?string $assetPairCode, ?int $active)
    {
        $body = [
            'asset_pair_code' => $assetPairCode,
            'maker_active'    => $active,
        ];

        return $this->makeCallGuzzle('PUT', 'maker_asset_pair', $body);
    }

    /**
     * Command for listening : get_assets_pairs_codes
     * Get list assets pairs codes
     * @return array
     */
    public function getAssetsPairsCodes()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'assets_pairs_codes', $body);
    }

    public function updateAsset(string $assetCode, array $assetData)
    {
        $body = [
            'asset_code' => $assetCode,
            'asset_data' => $assetData,
        ];

        return $this->makeCallGuzzle('PUT', 'update_asset', $body);
    }

    public function addToken(string $code)
    {
        $body = [
            'token_code' => $code
        ];

        return $this->makeCallGuzzle('POST', 'add_token', $body);
    }

    public function getTokens()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'tokens', $body);
    }

    public function getDataForGraph(string $pair, string $interval, int $chunk, string $toTs = null)
    {
        $body = [
            'pair'     => $pair,
            'interval' => $interval,
            'chunk'    => $chunk,
            'toTs'     => $toTs,
        ];

        return $this->makeCallGuzzle('GET', 'graph_data', $body);
    }

    public function getAssetInterest()
    {
        $body = [];

        return $this->makeCallGuzzle('GET', 'asset_interest', $body);
    }

    public function updateAssetInterest(array $assets)
    {
        $body = [
            'assets' => $assets
        ];

        return $this->makeCallGuzzle('PUT', 'update_asset_interest', $body);
    }


//    /**
//     *  Command for listening : ger_hot_wallet_balance
//     * Get hot wallet balances
//     * @return array
//     */
//    public function getHotWalletBalance()
//    {
//
//        $msg = [
//            'command' => __TRAIT__.'@'.__FUNCTION__,
//            'data' => [ ],
//        ];
//
//        return $this->makeCall($msg);
//    }
}