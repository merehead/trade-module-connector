<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/8/18
 * Time: 3:12 PM
 */

namespace MereHead\TradeModuleConnector\TradeServices;


trait OrdersService
{
    /**
     * Command for listening : getOrdersBook
     * Get orders book
     * @param string $pair it's get parameters like btc_ltc
     * @return mixed
     */
    public function getOrdersBook(string $pair, $count)
    {
        $body = [
            'pair' => $pair,
            'count' => $count,
        ];

        return $this->makeCallGuzzle('GET', 'orders_book', $body);
    }

    /**
     * Command for listening : getTrades
     * Get trades
     * @param string $pair it's get parameters like btc_ltc
     * @return mixed
     */
    public function getTrades(string $pair, $count)
    {
        $body = [
            'pair' => $pair,
            'count' => $count,
        ];

        return $this->makeCallGuzzle('GET', 'trades', $body);
    }


    /**
     * Command for listening : get_orders
     * Get user order
     * @param int $accountId
     * @param string|null $pair
     * @param string|null $status
     * @param int $current_page
     * @param int $per_page
     * @param bool $isMargin
     * @return mixed
     */
    public function getOrders(int $accountId, string $pair = null, string $status = null, int $current_page = 0, int $per_page = 15, bool $isMargin = false)
    {
        $body = [
            'account_id' => $accountId,
            'pair' => $pair,
            'status' => $status,
            'current_page' => $current_page,
            'per_page' => $per_page,
            'is_margin' => $isMargin
        ];

        return $this->makeCallGuzzle('GET', 'my_orders', $body);
    }

    /**
     * Command for listening : orders
     * Get all users orders
     * @param int $current_page
     * @param int $per_page
     * @param string $search_query
     * @return mixed
     */
    public function getAllOrders(int $current_page = 0, int $per_page = 15, string $search_query = null)
    {
        $body = [
            'current_page' => $current_page,
            'per_page'     => $per_page,
            'search_query' => $search_query,
        ];

        return $this->makeCallGuzzle('GET', 'orders', $body);
    }

    /**
     * Command for listening : get_transactions
     * Get user transactions
     * @param int $accountId
     * @param int $current_page
     * @param int $per_page
     * @param bool $isMargin
     * @return mixed
     */
    public function getTransactions(int $accountId, int $current_page = 0, int $per_page = 15, bool $isMargin = false)
    {
        $body = [
            'account_id' => $accountId,
            'current_page' => $current_page,
            'per_page' => $per_page,
            'is_margin' => $isMargin
        ];

        return $this->makeCallGuzzle('GET', 'orders_transactions', $body);
    }

    /**
     * Command for listening : calculate_limit_order
     * Calculate user limit order
     * @param int $accountId
     * @param string $type
     * @param string $pair
     * @param float $price
     * @param float $quantity
     * @return array
     */
    public function calculateLimitOrder(int $accountId, string $type, string $pair, float $price, float $quantity)
    {
        $body = [
            'account_id' => $accountId,
            'type'       => $type,
            'pair'       => $pair,
            'price'      => $price,
            'quantity'   => $quantity,
        ];

        return $this->makeCallGuzzle('POST', 'calculate_limit_order', $body);
    }

    public function publicCalculateLimitOrder(string $type, string $pair, float $price, float $quantity)
    {
        $body = [
            'type'       => $type,
            'pair'       => $pair,
            'price'      => $price,
            'quantity'   => $quantity,
        ];

        return $this->makeCallGuzzle('POST', 'public_calculate_limit_order', $body);
    }

    /**
     * Command for listening : calculate_market_order
     * Calculate market order
     * @param int $accountId
     * @param string $type
     * @param string $pair
     * @param float $quantity
     * @return array
     */
    public function calculateMarketOrder(int $accountId, string $type, string $pair, float $quantity)
    {
        $body = [
            'account_id' => $accountId,
            'type'       => $type,
            'pair'       => $pair,
            'quantity'   => $quantity,
        ];

        return $this->makeCallGuzzle('POST', 'calculate_market_order', $body);
    }

    public function publicCalculateMarketOrder(string $type, string $pair, float $quantity)
    {
        $body = [
            'type'       => $type,
            'pair'       => $pair,
            'quantity'   => $quantity,
        ];

        return $this->makeCallGuzzle('POST', 'public_calculate_market_order', $body);
    }

    /**
     * Command for listening : create_limit_order
     * Create user limit order
     * @param int $accountId
     * @param string $type
     * @param string $pair
     * @param float $price
     * @param float $quantity
     * @param bool $isMargin
     * @return array
     */
    public function createLimitOrder(int $accountId, string $type, string $pair, float $price, float $quantity, bool $isMargin): array
    {
        $data = [
            'account_id' => $accountId,
            'type' => $type,
            'pair_code' => $pair,
            'price' => $price,
            'quantity' => $quantity,
            'is_margin' => $isMargin,
        ];

        return $this->makeCallGuzzle('POST', 'create_limit_order', $data);
    }

    /**
     * Command for listening : create_market_order
     * Create market order
     * @param $accountId
     * @param $type
     * @param $pair
     * @param $quantity
     * @param $isMargin
     * @return array
     */
    public function createMarketOrder( $accountId, $type, $pair, $quantity, $isMargin): array
    {
        $data = [
            'account_id' => $accountId,
            'type' => $type,
            'pair_code' => $pair,
            'quantity' => $quantity,
            'is_margin' => $isMargin,
        ];
        return $this->makeCallGuzzle('POST', 'create_market_order', $data);
    }

    /**
     * Command for listening : cancel_order
     * Cancel user order
     * @param $user_id
     * @param $orderId
     * @return array
     */
    public function cancelOrder($user_id, $orderId)
    {
        $data = [
            'account_id' => $user_id,
            'order_id' => $orderId
        ];

        return $this->makeCallGuzzle('POST', 'cancel_order', $data);
    }

    /**
     * Command for listening : cancel_all_orders
     * Cancel all user orders
     * @param int $user_id
     * @param string $asset_pair_code
     * @param bool $isMargin
     * @return array
     */
    public function cancelAllOrders(int $user_id, string $asset_pair_code, bool $isMargin = false)
    {
        $data = [
            'account_id'       => $user_id,
            'asset_pair_code'  => $asset_pair_code,
            'is_margin'  => $isMargin,
        ];

        return $this->makeCallGuzzle('POST', 'cancel_all_orders', $data);
    }

    public function createStopLimitOrder(int $accountId, float $stop, float $limit, float $amount, string $pair, string $type, bool $isMargin): array
    {
        $data = [
            'account_id' => $accountId,
            'stop' => $stop,
            'limit' => $limit,
            'amount' => $amount,
            'pair' => $pair,
            'type' => $type,
            'is_margin' => $isMargin,
        ];

        return $this->makeCallGuzzle('POST', 'create_stop_limit_order', $data);
    }

    public function cancelStopLimitOrder(int $accountId, int $orderId): array
    {
        $data = [
            'account_id' => $accountId,
            'order_id' => $orderId
        ];

        return $this->makeCallGuzzle('POST', 'cancel_stop_limit_order', $data);
    }
}
