<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 8/15/18
 * Time: 4:44 PM
 */

namespace MereHead\TradeModuleConnector\TradeServices;


trait BalanceService
{
    /**
     * Command for listening : get_balances
     * Get user balance
     * @param int $accountId
     * @return mixed
     */
    public function getBalances(int $accountId)
    {
        $body = [
            'account_id' => $accountId,
        ];

        return $this->makeCallGuzzle('GET', 'balances', $body);
    }


    /***
     * Set balance for user
     * @param int $accountId
     * @param int $assetId
     * @param $balance
     * @return mixed
     */
    public function setBalance(int $accountId, int $assetId, $balance)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'balance' => $balance,
        ];

        return $this->makeCallGuzzle('POST', 'set_balance', $body);
    }

    /**
     * Get balance user for specific asset
     * @param int $accountId
     * @param int $assetId
     * @return mixed
     */
    public function getBalance(int $accountId, int $assetId)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
        ];

        return $this->makeCallGuzzle('GET', 'get_balance', $body);
    }

    /**
     * Set frozen balance for user
     * @param int $accountId
     * @param int $assetId
     * @param $amount
     * @param string|null $type (needs for adding withdrawal transaction to table with deposits)
     * @return mixed
     */
    public function setFrozenBalance(int $accountId, int $assetId, $amount, string $type = null)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
            'type' => $type,
        ];

        return $this->makeCallGuzzle('POST', 'set_frozen_balance', $body);
    }

    /**
     * Command for listening : increase_balance
     * Increase user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @param string|null $type  deposit or withdraw
     * @return mixed
     */
    public function increaseBalance(int $assetId, int $accountId, float $amount, string $type = null)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
            'type' => $type,
        ];

        return $this->makeCallGuzzle('POST', 'increase_balance', $body);
    }


    /**
     * Command for listening : freeze_balance
     * Freeze user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function freezeBalance(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'freeze_balance', $body);
    }

    public function restoreBalance(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'restore_balance', $body);
    }

    /**
     * Command for listening : decrease_balance
     * Decrease user balance
     * @param int $assetId
     * @param int $accountId
     * @param float $amount
     * @return mixed
     */
    public function decreaseBalance(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'decrease_balance', $body);
    }

    public function getUsersBalances(string $assetId = null, $all = null)
    {
        $body = [
            'asset_id' => $assetId,
            'all'      => $all,
        ];

        return $this->makeCallGuzzle('GET', 'users_balances', $body);
    }

    public function getMarginBalance(int $accountId, int $assetId)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
        ];

        return $this->makeCallGuzzle('GET', 'get_margin_balance', $body);
    }

    public function getMarginBalances(int $accountId)
    {
        $body = [
            'account_id' => $accountId,
        ];

        return $this->makeCallGuzzle('GET', 'margin_balances', $body);
    }

    public function getBorrow(int $assetId, int $accountId)
    {
        $body = [
            'asset_id' => $assetId,
            'account_id' => $accountId,
        ];

        return $this->makeCallGuzzle('GET', 'get_borrow', $body);
    }

    public function getBorrows(int $accountId, int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'account_id' => $accountId,
            'current_page' => $current_page,
            'per_page' => $per_page,
        ];

        return $this->makeCallGuzzle('GET', 'get_borrows', $body);
    }

    public function getRepay(int $assetId, int $accountId)
    {
        $body = [
            'asset_id' => $assetId,
            'account_id' => $accountId,
        ];

        return $this->makeCallGuzzle('GET', 'get_repay', $body);
    }

    public function getRepays(int $accountId, int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'account_id' => $accountId,
            'current_page' => $current_page,
            'per_page' => $per_page,
        ];

        return $this->makeCallGuzzle('GET', 'get_repays', $body);
    }

    public function transferToMargin(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'transfer_to_margin', $body);
    }

    public function transferToExchange(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'transfer_to_exchange', $body);
    }

    public function borrowingMarginBalances(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'borrowing_margin_balances', $body);
    }

    public function repayMarginBalances(int $assetId, int $accountId, float $amount)
    {
        $body = [
            'account_id' => $accountId,
            'asset_id' => $assetId,
            'amount' => $amount,
        ];

        return $this->makeCallGuzzle('POST', 'repay_margin_balances', $body);
    }

    public function lowRisk(int $accountId)
    {
        $body = [
            'account_id' => $accountId,
        ];

        return $this->makeCallGuzzle('GET', 'low_risk', $body);
    }

    public function getUserMarginBalanceHistory(int $accountId, int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'account_id' => $accountId,
            'current_page' => $current_page,
            'per_page' => $per_page,
        ];

        return $this->makeCallGuzzle('GET', 'user_margin_balance_history', $body);
    }

    public function getInterestHistory(int $accountId, int $current_page = 0, int $per_page = 15)
    {
        $body = [
            'account_id' => $accountId,
            'current_page' => $current_page,
            'per_page' => $per_page,
        ];

        return $this->makeCallGuzzle('GET', 'interest_history', $body);
    }



//    /**
//     * Get user wallets
//     * @param int $accountId
//     * @return mixed
//     */
//    public function getUserWalletsByBalance(int $accountId){
//        $msg = [
//            'command' => __TRAIT__.'@'.__FUNCTION__,
//            'data' => [
//                'account_id' => $accountId,
//            ],
//        ];
//        return $this->makeCall($msg);
//    }
}