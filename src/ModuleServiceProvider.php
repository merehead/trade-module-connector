<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/26/18
 * Time: 12:46 PM
 */

namespace MereHead\TradeModuleConnector;


use Illuminate\Support\ServiceProvider;
use MereHead\TradeModuleConnector\Modules\TradeModuleService;

class ModuleServiceProvider extends ServiceProvider
{
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $config = __DIR__.'/Config/config.php';

        $this->publishes([
            $config => config_path('trademoduleconnector.php'),
        ], 'config');

        $this->mergeConfigFrom($config, 'trademoduleconnector');
    }


    /**
     * @throws \Exception
     */
    public function boot() {
        $this->app->bind('trademodule', TradeModuleService::class);
    }

}