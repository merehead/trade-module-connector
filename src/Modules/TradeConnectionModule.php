<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 7/19/18
 * Time: 2:51 PM
 */

namespace  MereHead\TradeModuleConnector\Modules;

use function Couchbase\defaultDecoder;
use GuzzleHttp\Client;

/**
 * Class TradeConnectionModule
 * @package App\Services\Modules
 */
class TradeConnectionModule
{

    protected $requester;

    //default connection to Trade-module
    private $encrypter;

    protected $guzzleClient;

    //check is connected
    protected $connected = false;

    function __construct()
    {
        if(config('trademoduleconnector.encryption_key')){
            $this->encrypter = new \Illuminate\Encryption\Encrypter(config('trademoduleconnector.encryption_key'), 'AES-256-CBC');
        }

        $this->guzzleClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => config('trademoduleconnector.tradeModuleDns'),//'http://localhost:82/api/',
        ]);
    }



    public function makeCallGuzzle(string $method, string $url, array $data)
    {
        $body = [];
        $data = json_encode($data);
        if(config('trademoduleconnector.encryption_key')){
            $data = $this->encrypter->encrypt($data);
        }

        $body['encrypted_data'] = $data;

        try {
            $response = $this->guzzleClient->request(
                $method,
                '/api/' . $url,
                [
                    'headers' => ['content-type' => 'application/json'],
                    'body'    => json_encode($body),
                ]
            );

            return json_decode($response->getBody(), 1);
        } catch(\Exception $e) {
            $response = $e->getResponse();

            if (null !== $response) {
                $res  = $response->getBody()->getContents();
                //Cheat to forward exceptions to main site
                if (env('APP_DEBUG')) {
                    dd($res);
                }
                dd('error in trade module');
            }
        }
    }
}