<?php

namespace  MereHead\TradeModuleConnector\Modules;
use MereHead\TradeModuleConnector\TradeServices\MarketDataService;
use MereHead\TradeModuleConnector\TradeServices\OrdersService;
use MereHead\TradeModuleConnector\TradeServices\PVCScreener;
use MereHead\TradeModuleConnector\TradeServices\TestService;
use MereHead\TradeModuleConnector\TradeServices\UserService;
use MereHead\TradeModuleConnector\TradeServices\AssetsService;
use MereHead\TradeModuleConnector\TradeServices\ExchangeService;
use MereHead\TradeModuleConnector\TradeServices\BalanceService;
use MereHead\TradeModuleConnector\TradeServices\ProfitService;

/**
 * This class using for sending data to server
 * Class TradeModuleService
 * @package Modules
 */
class TradeModuleService extends TradeConnectionModule
{

    use AssetsService, OrdersService, UserService, ExchangeService, BalanceService, ProfitService, TestService, MarketDataService, PVCScreener;


    /**
     * Command for listening : ping
     * Pinging trade module server
     * @return mixed
     */
    public function ping() {
        $msg = [
            'command' => 'ping',
        ];
        return $this->makeCall($msg);
    }

}